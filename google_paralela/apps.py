from django.apps import AppConfig


class GoogleParalelaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'google_paralela'

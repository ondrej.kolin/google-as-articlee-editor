from django import forms

class ArticleCreateForm(forms.Form):
    document_id = forms.CharField(max_length=255)
from googleapiclient.errors import HttpError
from googleapiclient.discovery import build
import re

named_style_html_mapping = {
    "NORMAL_TEXT": "p",
    "NAMED_STYLE_TYPE_UNSPECIFIED": "p",
    "TITLE": "h1",
    "SUBTITLE": "h2",
    "HEADING_1": "h3",
    "HEADING_2": "h4",
    "HEADING_3": "h5",
    "HEADING_4": "h6",
    "HEADING_5": "h6",
    "HEADING_6": "h6",
}

def _paragraph_element_solver(paragraph_element: dict) -> str:
    """
    Paragraph element is a part of the paragraph object, can be a different thing. Text, image, etc.
    https://developers.google.com/docs/api/reference/rest/v1/documents#ParagraphElement
    :param paragraph_element:
    :return:
    """
    if 'textRun' in paragraph_element:
        return paragraph_element['textRun'].get("content")
    elif 'inlineObjectElement' in paragraph_element:
        return f"[MEDIA#{paragraph_element['inlineObjectElement']['inlineObjectId']}]"
    # Others are not implemented ATM
    return ""

def _inline_element_solver(inline_element: dict) -> str:
    """
    Solves an inline element object and makes it HTML
    https://developers.google.com/docs/api/reference/rest/v1/InlineObject
    :param inline_element:
    :return:
    """
    # SOLVE POSITIONING OF THE IMAGES
    if not "imageProperties" in inline_element['inlineObjectProperties']['embeddedObject']:
        return f"<!-- Not supported (MEDIA #{inline_element['objectId']}) -->"
    embeddedObject = inline_element["inlineObjectProperties"]["embeddedObject"]
    # Compute width, height
    height, width = [round(metric['magnitude'], 2) for metric in embeddedObject['size'].values()]
    print(width, height)
    return f'<img class="media-inline" ' \
           f'src="{embeddedObject["imageProperties"]["contentUri"]}" ' \
           f'style="width: {width}pt; height: {height}pt;"' \
           f'/>'

def _positioned_element_solver(positioned_element: dict) -> str:

    try:
        object_properties = positioned_element["positionedObjectProperties"]
        height, width = [round(metric['magnitude'], 2) for metric in object_properties['embeddedObject']['size'].values()]
        return f'<img class="media-positioned' \
               f'positioned-object-layout-{object_properties["positioning"]["layout"].lower()}"' \
               f'style="width: {width}pt; height: {height}pt;"' \
               f'src="{object_properties["embeddedObject"]["imageProperties"]["contentUri"]}"' \
               f'/>'
    except KeyError as e:
        print("POSITIONED ELEMENT NOT RENDERED! ", e)
        return f"<!-- Not supported (POSITIONEDELEMENT #{positioned_element['objectId']}) -->"



def _shard_to_html(shard: dict) -> str:
    """
    Shard is a document->Structural item, see the documentation.
    https://developers.google.com/docs/api/reference/rest/v1/documents#StructuralElement
    :param shard:
    :return:
    """
    # Only handling paragraph items
    if not "paragraph" in shard:
        return ""
    # Element is a google document -> paragraph -> paragraph element
    # https: // developers.google.com / docs / api / reference / rest / v1 / documents  # ParagraphElement
    namedStyleType = shard['paragraph']['paragraphStyle'].get("namedStyleType", "NORMAL_TEXT")
    html_tag = named_style_html_mapping[namedStyleType]
    # Transform parts of the paragraph to a HTML texts (+ extra custom macros)
    # Parse elements
    # document -> paragraph -> elements
    html_paragraph_elements: list[str] = []
    element: dict
    for element in shard['paragraph'].get("elements", []):
        element = _paragraph_element_solver(element)
        html_paragraph_elements.append(element)
    output = f"<{html_tag}>{''.join(html_paragraph_elements)}</{html_tag}>"
    positioned_objects_html = []
    for positionedObjectId in shard['paragraph'].get("positionedObjectIds", []):
        positioned_objects_html.append(f"[POSITIONEDOBJECT#{positionedObjectId}]")
    if positioned_objects_html:
        output += f"<div class='positioned-items'>{''.join(positioned_objects_html)}</div>"
    return output


def google_doc_to_html(document: dict):
    """
    Converts Google document (response resource from the API) to a simple HTML form. Not much formatting is supported
    :param document:
    :return:
    """
    content: list = document.get('body')['content']
    result: list = []
    print(f"Document {document.get('documentId')}\nRevision {document.get('revisionId')}")
    for num, shard in enumerate(content):
        html = _shard_to_html(shard)
        # If it was rendered somehow
        if html:
            result.append(html)
        else:
            print(f"Skipping shard#{num} as it returned empty HTML")
            continue
    # Make the result a HTML
    html_result = "\n".join(result)
    # Solve inlineObjects
    for id, inlineObject in document.get('inlineObjects', {}).items():
        inlineObject = _inline_element_solver(inlineObject)
        html_result = re.sub(f"\[MEDIA#{id}\]", inlineObject, html_result)
        # Solve inlineObjects
    for id, positionedObject in document.get('positionedObjects', {}).items():
        positionedObject = _positioned_element_solver(positionedObject)
        html_result = re.sub(f"\[POSITIONEDOBJECT#{id}\]", positionedObject, html_result)
    return html_result

def fetch_google_doc(document_id:str, creds):
    service = build('docs', 'v1', credentials=creds)
    try:
        return service.documents().get(documentId=document_id).execute()
    except HttpError as e:
        print(e)
        return None
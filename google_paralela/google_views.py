# Django imports
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect
from django.template.response import TemplateResponse
from django.conf import settings
# Google imports
from google_auth_oauthlib.flow import InstalledAppFlow
from google.oauth2.credentials import  Credentials
from google.auth.transport.requests import Request
from googleapiclient.discovery import build
## Errors
from oauthlib.oauth2.rfc6749.errors import InvalidGrantError
# Top level imports
import pickle
# Local imports
from . import models
from . import google_utils


def google_authenticate(request: HttpRequest):
    try:
        credentials_obj: models.GoogleCredentials = models.GoogleCredentials.objects.all()[0]
        creds: Credentials = pickle.loads(credentials_obj.pickled_creds)
    except IndexError:
        creds: Credentials = None
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                settings.GOOGLE_APP_CREDENTIALS,
                settings.GOOGLE_APP_SCOPES,
            )
            flow.redirect_uri = "https://patreon.paralelnilisty.cz/google/callback"
            auth_url, _ = flow.authorization_url()
            return HttpResponseRedirect(redirect_to=auth_url)
    credentials_obj.pickled_creds = pickle.dumps(creds)
    credentials_obj.save()
    return HttpResponse("Hey you are valid :) ")

def google_renew(request: HttpRequest):
    """
    Dump all Google Credentials and request new one
    :param request:
    :return:
    """
    models.GoogleCredentials.objects.all().delete()
    return HttpResponseRedirect(redirect_to="/google/authenticate")

def google_callback(request: HttpRequest):
    """
    Callback endpoint of google oauth 2 request
    :param request:
    :return:
    """
    code: str = request.GET.get('code')
    if not str:
        return HttpResponse('Unfortunate, Google Authentication code is missing')
    flow = InstalledAppFlow.from_client_secrets_file(
        settings.GOOGLE_APP_CREDENTIALS,
        settings.GOOGLE_APP_SCOPES,
    )
    flow.redirect_uri = "https://patreon.paralelnilisty.cz/google/callback"
    try:
        flow.fetch_token(code=code)
    except InvalidGrantError:
        return HttpResponse("Error: token fetch", 401)
    creds_object: models.GoogleCredentials = models.GoogleCredentials()
    creds_object.pickled_creds = pickle.dumps(flow.credentials)
    creds_object.save()
    return HttpResponse("obtained")

def google_docs_list(request: HttpRequest) -> HttpResponse:
    # List all google docs documents you have :)
    creds: Credentials = models.GoogleCredentials.get_active_creds()
    if not creds:
        return HttpResponse(
            "It's sad, but you have no working credentials. RIP, <a href=\"/google/authenticate\">Login here :]</a>")
    service = build('drive', 'v3', credentials=creds)
    results = service.files().list(pageSize=100, fields="nextPageToken, files(id, name)").execute()
    # Build the response file
    response = TemplateResponse(request,
                               'google/docs.list',
                               {"files": results.get('files', [])})
    return response

def google_docs_detail(request: HttpRequest, document_id:str):
    '''
    Check if documents exists and CAN be displayed in HTML, display the document`
    :param request:
    :param document_id:
    :return:
    '''
    creds: Credentials = models.GoogleCredentials.get_active_creds()
    if not creds:
        return HttpResponse(
            "It's sad, but you have no working credentials. RIP, <a href=\"/google/authenticate\">Login here :]</a>")
    document = google_utils.fetch_google_doc(document_id, creds)
    if not document:
        return HttpResponse("I am sorry, but this document seems not to be working", 400)
    return TemplateResponse(request,
                                'google/docs.detail',
                                {"document": document,
                                 "content": google_utils.google_doc_to_html(document)})
# Generated by Django 3.2.4 on 2021-06-19 17:30

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(help_text='Název článku', max_length=255)),
                ('google_document_id', models.CharField(help_text='ID dokumentu v Google Docs', max_length=255)),
                ('content', models.TextField(help_text='Obsah článku')),
            ],
        ),
        migrations.CreateModel(
            name='GoogleCredentials',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pickled_creds', models.BinaryField(max_length=2048)),
            ],
        ),
    ]

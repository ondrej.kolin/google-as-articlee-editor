from django.db import models
from google.oauth2.credentials import  Credentials
import pickle

# Create your models here.
class Article(models.Model):
    title = models.CharField(max_length=255, help_text="Název článku")
    google_document_id = models.CharField(max_length=255, help_text="ID dokumentu v Google Docs")
    content = models.TextField(help_text="Obsah článku")

class GoogleCredentials(models.Model):
    pickled_creds = models.BinaryField(max_length=2048)

    @staticmethod
    def get_active_creds() -> Credentials:
        """
        Return working google credentails
            OR return None if no such credentials exists
        :param self:
        :return:
        """
        creds : GoogleCredentials
        for creds in GoogleCredentials.objects.all():
            google_creds: Credentials = pickle.loads(creds.pickled_creds)
            if google_creds and google_creds.valid and not google_creds.expired:
                return google_creds

from django.urls import path
from . import google_views
from . import views

urlpatterns = [
    # Google based views
    path('google/authenticate/', google_views.google_authenticate, name="google/authenticate"),
    path('google/renew/', google_views.google_renew, name="google/renew"),
    path('google/callback/', google_views.google_callback),
    path('google/docs/', google_views.google_docs_list, name="google/docs"),
    path('google/docs/<str:document_id>/', google_views.google_docs_detail, name="google/docs/detail"),
    # Local views
    path('article/create/', views.ArticleCreateView.as_view(), name="article/create"),
    path('article/<pk>/update', views.article_update, name="article/update"),
    path('article/<pk>/', views.ArticleView.as_view(), name="article/detail"),
    path('article/', views.ArticleList.as_view(), name="article/list")

]
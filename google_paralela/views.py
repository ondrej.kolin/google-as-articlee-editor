from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import FormView
from google.oauth2.credentials import  Credentials
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from . import forms, models, google_utils

# Create your views here.
class ArticleCreateView(FormView):
    template_name = "article/create"
    form_class = forms.ArticleCreateForm
    success_url = "article/"
    article_id = None

    def get_success_url(self):
        if self.article_id:
            return f"{self.success_url}/{self.article_id}"
        else:
            return self.success_url

    def form_valid(self, form:forms.ArticleCreateForm):
        if models.Article.objects.filter(google_document_id=form.cleaned_data['document_id']).exists():
            return HttpResponse("Hey! We've seen a document like this before, this ID is already in use ;)")
        creds: Credentials = models.GoogleCredentials.get_active_creds()
        if not creds:
            return HttpResponse("Requiers valid credentials. Maybe they are outdated?")
        # Fetch the document and translate it to html
        document: dict = google_utils.fetch_google_doc(form.cleaned_data['document_id'], creds)
        if document is None:
            return HttpResponse("Oh no! It's fucked up")
        html: str = google_utils.google_doc_to_html(document)
        # Build the document object
        article = models.Article(
            title = document.get("title"),
            google_document_id = document.get("documentId"),
            content = html
        )
        article.save()
        self.article_id = article.id
        return super().form_valid(form)

def article_update(request: HttpRequest, pk: str):
    article:models.Article = get_object_or_404(models.Article, id=pk)
    creds: Credentials = models.GoogleCredentials.get_active_creds()
    if not creds:
        return HttpResponse("Requiers valid credentials. Maybe they are outdated?")
    # Fetch the document and translate it to html
    document: dict = google_utils.fetch_google_doc(article.google_document_id, creds)
    if document is None:
        return HttpResponse("Oh no! It's fucked up")
    article.content = google_utils.google_doc_to_html(document)
    article.save()
    return HttpResponseRedirect(f"/article/{article.id}")

class ArticleView(DetailView):
    template_name = "article/detail"
    model = models.Article

class ArticleList(ListView):
    template_name = "article/list"
    model = models.Article